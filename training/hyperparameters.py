import os
import h5py
import numpy as np


def get_path(name):
    return os.path.dirname(os.path.dirname(__file__)) + '\\data_sampling\\' + name + '.h5'


def readfromh5(hf):
    dataX = []
    dataU = []

    for i, h in enumerate(hf):
        g = hf[h]

        for j, key in enumerate(g):
            dataX.append(np.swapaxes(g[key]["x_t"], 0, 1)[:, :])
            dataU.append(np.swapaxes(g[key]["u_t"][1:], 0, 1)[:, :])

        dataX = np.stack(dataX, axis=0)
        dataU = np.stack(dataU, axis=0)

    return dataX, dataU


def create_generic_example(n=(10, ), n_d=True, act="elu",):
    opts = {"n_x": 2, "n_u": 1, "n_z": 5}
    layers = [{"nodes": i_n, "activation": act} for i_n in n]
    opts["encoder"] = {"layers": layers}
    if n_d:
        opts["decoder"] = {"layers": layers[::-1]}
    else:
        opts["decoder"] = {"layers": ()}

    opts["bilinear"] = False
    opts["epochs"] = 10000
    opts["loss_weights"] = (.1, 1., 1., 1e-9)
    opts["num_steps_time"] = 50

    opts["lr"] = 0.001
    opts["batch_size"] = 32

    opts["split_val"] = 0.2
    opts["split_test"] = 0.0
    opts["scale"] = (True, True)

    return opts, None, None


def create_multiplicity_example(n=(20, 10, ), n_d=True, act="elu",):
    opts, _, _ = create_generic_example(n=n, n_d=n_d, act=act)
    opts["n_x"] = 2
    opts["n_u"] = 1
    opts["n_z"] = 1
    hf = h5py.File(get_path('ds_multiplicity'), 'r')
    dataX_raw, dataU_raw = readfromh5(hf)

    return opts, dataX_raw, dataU_raw


def create_reactor_example(n=(10, ), n_d=True, act="elu"):
    opts, _, _ = create_generic_example(n=n, n_d=n_d, act=act)
    opts["n_x"] = 2
    opts["n_u"] = 1
    opts["n_z"] = 1
    hf = h5py.File(get_path('ds_reactor'), 'r')
    dataX_raw, dataU_raw = readfromh5(hf)

    return opts, dataX_raw, dataU_raw


def create_pearson_example(n=(20, ), n_d=True, act="elu"):
    opts, _, _ = create_generic_example(n=n, n_d=n_d, act=act)
    opts["n_x"] = 10
    opts["n_u"] = 2
    opts["n_z"] = 2
    hf = h5py.File(get_path('ds_pearson'), 'r')
    dataX_raw, dataU_raw = readfromh5(hf)

    return opts, dataX_raw, dataU_raw
