import datetime as datetime
import os as os
from tensorflow.keras.callbacks import ModelCheckpoint
from sklearn.preprocessing import MinMaxScaler
# Own packages
from koopman.hw_net import *


def stack_data(xdatain, udatain, **kwargs):
    """
    Creates multiple shorter trajectories for training
    :param xdata: State data in format (experiment, time series, states)
    :param udata: Input data in format (experiment, time series, inputs)
    :param kwargs:
    :return: Stacked time frames in form (time frames of experiements, time series of window length, states/inputs)
    """

    # (sample, time, state)
    num_shifts = kwargs["num_shifts"]  # number of windows created per data set
    len_time = kwargs["len_time"]  # total length of one window

    take_horizon = int(num_shifts * len_time)

    xdata = xdatain[:, :take_horizon, :]
    udata = udatain[:, :take_horizon, :]

    xdata = tf.reshape(xdata, shape=(-1, xdata.shape[2]))
    udata = tf.reshape(udata, shape=(-1, udata.shape[2]))

    outX1 = tf.reshape(xdata, shape=(-1, len_time, xdata.shape[1]))
    outU1 = tf.reshape(udata, shape=(-1, len_time, udata.shape[1]))

    return outX1, outU1


def scale_data(dataX_raw, dataU_raw, scale=(False, False)):
    scalerX = MinMaxScaler(feature_range=(0., 1.))
    scalerU = MinMaxScaler(feature_range=(0., 1.))

    # Scale states
    if scale[0]:
        dataX_resh = np.reshape(dataX_raw, (dataX_raw.shape[0] * dataX_raw.shape[1], dataX_raw.shape[2]))
        scalerX.fit(dataX_resh)

        # Some consistency checks for reshaping
        assert np.count_nonzero(np.reshape(dataX_resh, dataX_raw.shape) - dataX_raw) == 0

        # Scale data
        dataX = np.reshape(scalerX.transform(dataX_resh), dataX_raw.shape)

    else:
        dataX = dataX_raw

    # Scale inputs
    if scale[1]:
        dataU_resh = np.reshape(dataU_raw, (dataU_raw.shape[0] * dataU_raw.shape[1], dataU_raw.shape[2]))
        scalerU.fit(dataU_resh)

        # Some consistency checks
        assert np.count_nonzero(np.reshape(dataU_resh, dataU_raw.shape) - dataU_raw) == 0

        dataU = np.reshape(scalerU.transform(dataU_resh), dataU_raw.shape)
    else:
        dataU = dataU_raw

    return dataX, dataU, (scalerX, scalerU)


def create_datasets(dataX_scaled, dataU_scaled, opts):
    """
    Takes numpy data arrays and creates tensorflow datasets for training and validation
    """
    assert dataX_scaled.shape[0] == dataU_scaled.shape[0]
    assert dataX_scaled.shape[1] == dataU_scaled.shape[1]

    dataX_scaled = tf.constant(dataX_scaled, dtype=dtype_f)
    dataU_scaled = tf.constant(dataU_scaled, dtype=dtype_f)

    dataX, dataU = stack_data(dataX_scaled, dataU_scaled,
                              num_shifts=opts["num_shifts"],
                              len_time=opts["len_time"])

    data = tf.concat((dataX, dataU), axis=2)
    dataset = tf.data.Dataset.from_tensor_slices(data)
    dataset = dataset.shuffle(buffer_size=int(1E6), reshuffle_each_iteration=True)

    dataset_batch = dataset.batch(opts["batch_size"], drop_remainder=True)
    num_batches = len(dataset_batch)

    train_size = int((1 - opts["split_val"] - opts["split_test"]) * num_batches)
    val_size = int(opts["split_val"] * num_batches)

    train_ds = dataset_batch.take(train_size)
    val_ds = dataset_batch.skip(train_size).take(val_size)
    test_ds = dataset_batch.skip(train_size).skip(val_size)

    return (train_ds, val_ds, test_ds), dataset


def run_example(opts, dataX_raw, dataU_raw):

    dataX_scaled, dataU_scaled, (scalerX, scalerU) = scale_data(dataX_raw, dataU_raw, scale=opts["scale"])

    assert dataX_scaled.shape[-1] == opts["n_x"]
    assert dataU_scaled.shape[-1] == opts["n_u"]

    # Create Koopman model
    koopman = KoopmanModel(opts)

    # Update options after Koopman configuration
    opts["len_time"] = koopman.len_time   # Trajectory length
    opts["num_shifts"] = int(np.floor(dataX_scaled.shape[1] / koopman.len_time))  # No of trajectories

    (train_ds, val_ds, test_ds), dataset = create_datasets(dataX_scaled, dataU_scaled, opts)

    ###########################################################
    # SET TRAINING OPTIONS
    # Optimizer
    opt = tf.keras.optimizers.Adam(learning_rate=opts["lr"])

    # Specify Callbacks
    checkpoint_filepath = get_checkpoint_path('checkpoint' + datetime.datetime.now().strftime("%m%d_%H%M_%S%f"))

    model_check_point = ModelCheckpoint(monitor="val_loss",
                                        mode='min',
                                        save_freq="epoch",
                                        save_best_only=True,
                                        save_weights_only=True,
                                        filepath=checkpoint_filepath,
                                        )

    ###########################################################
    # MAIN TRAINING
    print("\n\nTrain Koopman-Wiener")

    koopman.ssm.trainable = True
    koopman.encoder.trainable = True
    koopman.decoder.trainable = True

    koopman.compile(optimizer=opt)
    koopman.run_eagerly = False

    history = koopman.fit(x=train_ds, epochs=opts["epochs"],
                          validation_data=val_ds,
                          callbacks=[model_check_point, ])

    with open("mylog.txt", "a") as file1:
        for i in opts:
            file1.write(i + ': ' + str(opts[i]) + "\n")
        file1.write("\n" + checkpoint_filepath + "\n")
        file1.write('Best validation loss: {} \n\n'.format(min(history.history["val_loss"])))

    print("Done\n\n\n")


def results_path(name):
    return os.path.dirname(os.path.dirname(os.path.dirname(__file__))) + "\\" + name + ".txt"


def get_checkpoint_path(name):
    return os.path.dirname(os.path.dirname(__file__)) + '\\weights\\' + name

