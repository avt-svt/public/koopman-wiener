###########################################################
# IMPORT PACKAGES
import copy
import tensorflow as tf
import numpy as np
from tqdm import tqdm

print(tf.__version__)

##################################################
# Some basic settings
prec = 32
eps = 1e-16

if prec == 32:
    dtype_f = tf.dtypes.float32
    dtype_i = tf.dtypes.int32
    np_dtype_f = np.float32
else:
    dtype_f = tf.dtypes.float64
    dtype_i = tf.dtypes.int64
    np_dtype_f = np.float64


##################################################
# Loss functions
def basic_loss(pred, data, rel=True):
    # Mean squared error
    error = tf.math.subtract(data, pred)
    loss_numerator = tf.math.reduce_mean(tf.reduce_mean(tf.square(error), 1))
    if rel:
        loss_denominator = eps + tf.math.reduce_mean(tf.reduce_mean(tf.square(data), 1))
    else:
        loss_denominator = tf.constant(1.0, dtype=dtype_f)

    mse = tf.math.truediv(loss_numerator, loss_denominator)

    return (loss_numerator, loss_denominator), mse


def regularization_loss(trainable_vars, num_vars=1.0):
    # Implements the l1 loss function
    loss = tf.zeros([1, ])
    for i in trainable_vars:
        loss = loss + tf.math.reduce_sum(tf.math.abs(i))
    loss = tf.math.truediv(loss, num_vars)
    return loss


##################################################
# Linear and Bilinear State space model class
class SSM(tf.keras.layers.Layer):
    """
    State space model to be part of Koopman model
    """
    def __init__(self, dim_x, dim_u, bilinear=False, ker_init=None, dtype=dtype_f):
        super(SSM, self).__init__()

        self.dim_u = dim_u
        self.dim_x = dim_x
        self.bilinear = bilinear

        self.wx = self.add_weight(shape=(self.dim_x, ),
                                  name="wx_SSM",
                                  initializer=ker_init,
                                  trainable=True,
                                  dtype=dtype)

        self.wu = self.add_weight(shape=(self.dim_u, self.dim_x),
                                  name="wu_SSM",
                                  initializer=ker_init,
                                  trainable=True,
                                  dtype=dtype)

        if self.bilinear:
            ker_bil_init = ker_init
            self.wux = self.add_weight(shape=(self.dim_u, self.dim_x, self.dim_x),
                                       name="wux_SSM",
                                       initializer=ker_bil_init,
                                       trainable=True,
                                       dtype=dtype)

    def call(self, inputs, *args, **kwargs):
        inputs_x, inputs_u = inputs

        # Ax
        x1 = tf.math.multiply(inputs_x, self.wx)

        # Bu
        x2 = tf.linalg.matmul(inputs_u, self.wu)

        # B' x u_i
        if self.bilinear:
            A2 = tf.tensordot(inputs_u, self.wux, [[1], [0]])
            x2 = tf.math.add(x2, tf.linalg.matvec(A2, inputs_x))

        x = tf.math.add(x1, x2)

        return x


class KoopmanModel(tf.keras.Model):
    def __init__(self, options):
        super(KoopmanModel, self).__init__()

        self.n_x = options["n_x"]  # full states
        self.n_z = options["n_z"]  # latent states
        self.n_u = options["n_u"]  # controls

        self.layernodes = [i["nodes"] for i in options["encoder"]["layers"]]
        self.layernodes_d = [i["nodes"] for i in options["decoder"]["layers"]]

        self.loss_weights = tf.constant(options["loss_weights"], dtype=dtype_f)
        self.useLoss = [ii > 0 for ii in options["loss_weights"][:-1]]

        self.len_time = options['num_steps_time']

        self.settingscalled = False

        # Kernel initializer
        kernel_init = tf.keras.initializers.VarianceScaling(scale=1.0, mode="fan_in",
                                                            distribution="untruncated_normal")
        bias_init = tf.keras.initializers.Zeros()

        # Encoder
        optsEncoder = options["encoder"]

        inpEncoder = x = tf.keras.layers.Input(shape=(self.n_x,), dtype=dtype_f)
        for layer in optsEncoder["layers"]:
            x = tf.keras.layers.Dense(layer["nodes"],
                                      activation=layer["activation"],
                                      kernel_initializer=kernel_init,
                                      bias_initializer=bias_init,
                                      dtype=dtype_f)(x)

        out = tf.keras.layers.Dense(self.n_z,
                                    activation='linear',
                                    kernel_initializer=kernel_init,
                                    bias_initializer=bias_init,
                                    dtype=dtype_f)(x)

        self.encoder = tf.keras.Model(inpEncoder, out, name="encoder")

        # Decoder
        optsDecoder = options["decoder"]

        inp = x = tf.keras.layers.Input(shape=(self.n_z,), dtype=dtype_f)
        for layer in optsDecoder["layers"]:
            x = tf.keras.layers.Dense(layer["nodes"],
                                      activation=layer["activation"],
                                      kernel_initializer=kernel_init,
                                      bias_initializer=bias_init,
                                      dtype=dtype_f)(x)

        out = tf.keras.layers.Dense(self.n_x,
                                    activation='linear',
                                    kernel_initializer=kernel_init,
                                    bias_initializer=bias_init,
                                    dtype=dtype_f)(x)

        self.decoder = tf.keras.Model(inp, out, name="decoder")

        # Linear state space model
        inp_x = tf.keras.layers.Input(shape=(self.n_z,), dtype=dtype_f)
        inp_u = tf.keras.layers.Input(shape=(self.n_u,), dtype=dtype_f)

        kernel_init = tf.keras.initializers.VarianceScaling(scale=0.01, mode="fan_in",
                                                            distribution="untruncated_normal")

        self.ssmLayer = SSM(dim_x=self.n_z, dim_u=self.n_u,
                            bilinear=options["bilinear"],
                            ker_init=kernel_init, dtype=dtype_f)

        out = self.ssmLayer((inp_x, inp_u))

        self.ssm = tf.keras.Model((inp_x, inp_u), out, name="state-space")

        # Num trainable vars in float format
        self.num_trainable_variables = tf.reduce_sum([tf.size(v, out_type=dtype_f) for v in
                                                      self.encoder.trainable_variables]) + \
                                       tf.reduce_sum([tf.size(v, out_type=dtype_f) for v in
                                                      self.ssm.trainable_variables]) + \
                                       tf.reduce_sum([tf.size(v, out_type=dtype_f) for v in
                                                      self.decoder.trainable_variables])

    def call(self, inputs, **kwargs):
        x_data, u_data = inputs[:, :self.n_x], inputs[:, self.n_x:]
        z = self.encoder(x_data)
        f = self.ssm((z, u_data))
        return self.decoder(f)

    def predict_multiple_steps(self, inputs, num_steps=None):
        x_data, u_data = inputs
        assert len(x_data.shape) == 2  # 2d array (time step, state)
        x0 = x_data[0:1, :]
        z_pred = self.encoder(x0)
        z_buffer = copy.deepcopy(z_pred)

        if num_steps is None:
            num_steps = u_data.shape[0]
        else:
            assert u_data.shape[0] <= num_steps

        for i in tqdm(range(0, num_steps)):
            z_buffer = tf.concat([z_buffer, self.ssm((z_buffer[-1:, :], u_data[i:i+1, :]))], axis=0)

            if i % 1000 == 0 and i > 0 or i == num_steps-1:
                # Addresses some speed limitation due to storage overhead
                z_pred = tf.concat([z_pred, z_buffer[1:, :]], axis=0)
                z_buffer = z_buffer[-1:, :]

        y_pred = self.decoder(z_pred[:, :])

        return y_pred

    def judge(self, xdata, udata):
        assert xdata.shape[0] == udata.shape[0] + 1

        inputs = (xdata, udata)
        xpred = self.predict_multiple_steps(inputs)
        _, accuracy = basic_loss(xpred, xdata)

        return xpred, accuracy

    def load_weights(self, filepath, by_name=False, skip_mismatch=False, options=None):
        print("Load weights from file")
        super(KoopmanModel, self).load_weights(filepath, by_name, skip_mismatch, options)

    def compile(self, **kwargs):
        super(KoopmanModel, self).compile(**kwargs)

    def test_step(self, data):
        x_data, u_data = data[:, :, :self.n_x], data[:, :, self.n_x:]

        assert x_data.shape[1] == u_data.shape[1]
        assert x_data.shape[0] == u_data.shape[0]

        test_loss, _ = self.compute_losses(x_data, u_data)

        return {"loss": test_loss}  # puts "val_" in front automatically

    @tf.function
    def compute_losses(self, x_data, u_data):
        self.useLoss = (True, True, True, True, True)

        batch_size = x_data.shape[0]

        x_data = x_data[:, :self.len_time, :]
        u_data = u_data[:, :self.len_time, :]

        # 1. Reconstruction Loss
        loss1N = tf.zeros([1, ], dtype=dtype_f)
        loss1D = tf.zeros([1, ], dtype=dtype_f)
        loss1 = tf.zeros([1, ], dtype=dtype_f)

        for ii in range(batch_size):
            xx = self.encoder(x_data[ii, :, :])
            xx_pred_ed = self.decoder(xx)
            (n, d), _ = basic_loss(xx_pred_ed, x_data[ii, :, :])
            loss1N = loss1N + n
            loss1D = loss1D + d

        loss1 = loss1 + tf.math.truediv(loss1N, loss1D)  # Divide in a last step

        # 2. Single-step prediction Loss
        loss2N = tf.zeros([1, ], dtype=dtype_f)
        loss2D = tf.zeros([1, ], dtype=dtype_f)
        loss2 = tf.zeros([1, ], dtype=dtype_f)

        if self.useLoss[1]:
            for ii in range(batch_size):
                z0 = self.encoder(x_data[ii, :-1, :])
                u0 = u_data[ii, :-1, :]
                z1 = self.ssm((z0, u0))
                (n, d), _ = basic_loss(self.decoder(z1), x_data[ii, 1:, :])
                loss2N = loss2N + n
                loss2D = loss2D + d

            loss2 = loss2 + tf.math.truediv(loss2N, loss2D)

        # 3. Multi-step Prediction Loss
        loss3N = tf.zeros([1, ], dtype=dtype_f)
        loss3D = tf.zeros([1, ], dtype=dtype_f)
        loss3 = tf.zeros([1, ], dtype=dtype_f)

        if self.useLoss[2]:
            # Linear multi-step prediction forward pass
            z0 = self.encoder(x_data[:, 0, :])  # First dataset of batch, slicing avoids reshape

            z_pred = tf.expand_dims(self.ssm((z0, u_data[:, 0, :])), axis=1)  # (s,x) -> (s,t=1,x)
            for ii in range(1, self.len_time - 1):
                z_pred = tf.concat([z_pred, tf.expand_dims(self.ssm((z_pred[:, -1, :], u_data[:, ii, :])), axis=1)],
                                   axis=1)

            for ii in range(batch_size):
                (n, d), _ = basic_loss(self.decoder(z_pred[ii, :, :]), x_data[ii, 1:, :])
                loss3N = loss3N + n
                loss3D = loss3D + d

            loss3 = loss3 + tf.math.truediv(loss3N, loss3D)

        # L1 Regularization loss for all parameters
        loss_reg = tf.zeros([1, ], dtype=dtype_f)
        if self.useLoss[3]:
            loss_reg = loss_reg + regularization_loss(self.trainable_variables, self.num_trainable_variables)

        # Sum up loss functions
        loss = (self.loss_weights[0] * loss1
                + self.loss_weights[1] * loss2
                + self.loss_weights[2] * loss3
                + self.loss_weights[3] * loss_reg
                )

        return loss, (loss1, loss2, loss3, loss_reg)

    @tf.function
    def train_step(self, data):
        """
        Overloads the training step part inside fit()
        Arguments:
            data -- training data tuples. Each x,u,y has shape (#samples, #timestepsPerSample, #dimX/U)
        """
        x_data, u_data = data[:, :, :self.n_x], data[:, :, self.n_x:]

        assert x_data.shape[1] == u_data.shape[1]
        assert x_data.shape[0] == u_data.shape[0]

        trainable_vars = self.trainable_variables

        # Gradient tape
        with tf.GradientTape() as tape:
            loss, (loss1, loss2, loss3, loss_reg) = self.compute_losses(x_data, u_data)

        # Evaluate gradient
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Return a dictionary that maps metric names to their current value
        return {"loss": loss, "loss_recon": loss1, "loss_single": loss2, "loss_multi": loss3, "loss_reg": loss_reg}
