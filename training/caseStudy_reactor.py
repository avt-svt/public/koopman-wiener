from koopman.experiments import *
import hyperparameters as ex


st = 0
for k in range(3):
    for _ in range(5):
        st += 1

        opts, dataX_raw, dataU_raw = ex.create_reactor_example((20, ), n_d=[True, False, False][k], act="elu")

        opts["n_z"] = 1
        opts["bilinear"] = [False, True, False][k]

        run_example(opts, dataX_raw, dataU_raw)
