# Identification of MIMO Wiener-type Koopman Models for Data-Driven Model Reduction using Deep-Learning
*Author: Jan C. Schulze (AVT.SVT)*

This respository contains the code for the paper [Identification of MIMO Wiener-type Koopman Models for Data-Driven Model Reduction using Deep-Learning](https://url) by J. Schulze, D. Doncevic and A. Mitsos.

The main functions of the deep-learning identification and reduction framework are contained in the folder "./training/koopman".

To run the case studies from the paper:
1. Create your training and test data set by running the respective Python script in the folder "./data_sampling".
2. Run the training procedure by executing the respective Python Case Study script in "./training". You may change the hyperparameters of the network structures in hyperparameters.py .
