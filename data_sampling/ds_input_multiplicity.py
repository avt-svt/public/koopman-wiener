import pyomo.environ as pyomo
import pyomo.dae as pydae
import numpy as np
from numpy import matlib
import matplotlib.pyplot as plt
import h5py

plt.ion()  # Show plots immediately
np.random.seed(42)


class SmallExample:
    """
    Model information see: https: https://doi.org/10.1371/journal.pone.0150171
    """
    def __init__(self, tsim=100.0):
        self.model = self.createModel(tf=tsim)
        self.inputs = pyomo.Suffix(direction=pyomo.Suffix.LOCAL)

    def setInputs(self, u):
        self.inputs[self.model.u] = {i_: j_ for i_, j_ in zip(u['t'], u['u'])}

    def simulateModel(self, initCond, ctr, ts=1.0):
        self.setInputs(ctr)
        opts = {'reltol': 1e-10, 'abstol': 1e-10}
        sim = pydae.Simulator(self.model, package="casadi")

        tt, xx = sim.simulate(tstep=ts, integrator="idas", initcon=initCond,
                              varying_inputs=self.inputs, integrator_options=opts)

        nn = list(map(str, sim.get_variable_order(vartype=None) + sim.get_variable_order(vartype='algebraic')))

        return tt, xx, nn

    def createModel(self, tf):
        model = pyomo.ConcreteModel()

        # Continuous Time Set
        t = model.t = pydae.ContinuousSet(bounds=(0.0, tf))

        # Physical Parameters
        model.mu = pyomo.Param(initialize=-0.1)
        model.lam = pyomo.Param(initialize=-1.)

        # Control
        model.u = pyomo.Var(t, initialize=1, bounds=(-10, 10))

        # Variables
        model.x1 = pyomo.Var(t, initialize=1, bounds=(-10, 10))
        model.x2 = pyomo.Var(t, initialize=1, bounds=(-10, 10))

        # Time derivatives
        model.dx1dt = pydae.DerivativeVar(model.x1)
        model.dx2dt = pydae.DerivativeVar(model.x2)

        # Dynamics
        model.f1 = pyomo.Constraint(t, rule=lambda m, k: model.dx1dt[k] == m.mu*m.x1[k] + m.u[k])
        model.f2 = pyomo.Constraint(t, rule=lambda m, k: model.dx2dt[k] == m.lam*(m.x2[k] - m.x1[k]**2))

        return model


if __name__ == "__main__":
    ts = 1.0
    tstep = np.array([200, 100])  # [Training, Testing]
    steps = np.array([100, 20])  # [Training, Testing]
    tf = np.multiply(tstep, steps)
    assert (tstep % ts == 0).all()

    for k in range(2):
        m1 = SmallExample(tsim=tf[k])

        if k == 0:
            hf = h5py.File('./ds_multiplicity.h5', 'w')
        else:
            hf = h5py.File('./ds_multiplicity_test.h5', 'w')

        hf.attrs['ts'] = ts
        hf.attrs['ns'] = 2  # Number of states
        hf.attrs['nu'] = 1  # Number of controls

        g = hf.create_group('scenario_{}'.format(0))

        controls = {'t': np.arange(start=0, stop=tf[k], step=tstep[k]), 'u': np.random.uniform(-1, 1, steps[k])}
        x0 = [1, 1]

        tim, output, names = m1.simulateModel(x0, controls, ts=ts)

        dataX = output[:, 0:2].transpose()
        dataU = np.vstack([np.arange(0, tf[k], ts), np.reshape(np.transpose(np.matlib.repmat(controls['u'], int(tstep[k]/ts), 1), (1, 0)), (1,-1))])

        gr = g.create_group('sample_{}'.format(0))
        gr.create_dataset('x_t', (2, int(tf[k] / ts)), dtype='f', data=dataX, compression="lzf")
        gr.create_dataset('u_t', (2, int(tf[k] / ts)), dtype='f', data=dataU, compression="lzf")

        hf.close()

        # Plot simulation results
        x = {name: output[:, i] for name, i in zip(names, range(output.shape[1]))}

        fig = plt.figure(k)
        ax = fig.subplots(3, 1)
        ax[0].stairs(controls['u'], np.append(controls['t'], tf[k]))
        ax[0].set_ylabel("u")
        ax[1].plot(tim, x['x1[{t}]'])
        ax[1].set_ylabel("x1")
        ax[2].plot(tim, x['x2[{t}]'])
        ax[2].set_ylabel("x2")

    print('Done.')
