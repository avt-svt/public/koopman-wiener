import pyomo.environ as pyomo  # also includes exp, log, etc
import pyomo.dae as pydae
import numpy as np
from numpy import matlib
import matplotlib.pyplot as plt
import h5py

plt.ion()  # Show plots immediately
np.random.seed(42)


class Reactor:
    """
    Model information see: https://doi.org/10.1002/aic.14683
    """
    def __init__(self, tsim=100.0):
        self.model = self.createModel(tf=tsim)
        self.inputs = pyomo.Suffix(direction=pyomo.Suffix.LOCAL)

    def setInputs(self, u):
        m = self.model
        for key in u:
            if key == 'Q':
                self.inputs[m.Q] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
            if key == 'F':
                self.inputs[m.F] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
            if key == 'CA0':
                self.inputs[m.CA0] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
            if key == 'Tr0':
                self.inputs[m.Tr0] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
        return

    def simulateModel(self, initCond, ctr, ts=1.0):
        self.setInputs(ctr)
        opts = {'reltol': 1e-6, 'abstol': 1e-6}
        sim = pydae.Simulator(self.model, package="casadi")

        tt, xx = sim.simulate(tstep=ts, integrator="idas", initcon=initCond,
                              varying_inputs=self.inputs, integrator_options=opts)

        nn = list(map(str, sim.get_variable_order(vartype=None) + sim.get_variable_order(vartype='algebraic')))
        return tt, xx, nn

    def createModel(self, tf):
        model = pyomo.ConcreteModel()

        # Continuous Time Set
        t = model.t = pydae.ContinuousSet(bounds=(0.0, tf))  # sec

        # Physical Parameters

        # Control
        model.CA0 = pyomo.Var(t, initialize=1, bounds=(0.5, 7.5))
        model.Tr0 = pyomo.Var(t, initialize=1, bounds=(0, 1))
        model.Q = pyomo.Var(t, initialize=1, bounds=(-5e5, 5e5))
        model.F = pyomo.Var(t, initialize=1, bounds=(-5e5, 5e5))

        # Variables
        model.CA = pyomo.Var(t, initialize=1, bounds=(0, 2))
        model.Tr = pyomo.Var(t, initialize=1, bounds=(300, 600))

        # Time derivatives
        model.dCAdt = pydae.DerivativeVar(model.CA)
        model.dTrdt = pydae.DerivativeVar(model.Tr)

        # Dynamics
        model.f1 = pyomo.Constraint(t, rule=lambda m, k: model.dCAdt[k] == m.F[k] / 1.0 * (
                model.CA0[k] - model.CA[k]) - 8.46e6 * pyomo.exp(-5e4 / (8.314 * m.Tr[k])) * m.CA[k]**2)

        model.f2 = pyomo.Constraint(t, rule=lambda m, k: model.dTrdt[k] == m.F[k] / 1.0 * (
                    model.Tr0[k] - model.Tr[k]) - -1.15e4 / (1000 * 0.231) * 8.46e6 * pyomo.exp(-5e4 / (8.314 * m.Tr[k])) * m.CA[k] ** 2 + m.Q[k] / (1000 * 0.231 * 1.0))

        return model


if __name__ == "__main__":
    ts = 1/60  # hrs
    generate_test_set = False
    if generate_test_set:
        nstep = 15
        tstep = 2.0  # hrs
    else:
        nstep = 500
        tstep = 4.0  # hrs
    tf = nstep*tstep
    assert tstep % ts < 1e-15

    m1 = Reactor(tsim=tf)

    shuf = np.arange(nstep)
    np.random.shuffle(shuf)

    if generate_test_set:
        hf = h5py.File('./ds_reactor_test.h5', 'w')
    else:
        hf = h5py.File('./ds_reactor.h5', 'w')

    hf.attrs['ts'] = ts
    hf.attrs['ns'] = 2  # no of states
    hf.attrs['nu'] = 1  # no of controls

    g = hf.create_group('scenario_{}'.format(0))

    x0 = [1.22, 438.2]
    Qmin = -2000
    Qmax = 10000

    controls = {'t': np.arange(start=0, stop=tf, step=tstep),
                'Q': np.linspace(Qmin, Qmax, nstep)[shuf],
                'CA0': [4.0],
                'F': [5.],
                'Tr0': [300.0]}

    tim, output, names = m1.simulateModel(x0, controls, ts=ts)

    dataX = output[:, :].transpose()
    dataU = np.vstack([np.arange(0, tf, ts),
                       np.reshape(np.transpose(np.matlib.repmat(controls['Q'], int(tstep/ts), 1), (1, 0)), (1, -1))])

    gr = g.create_group('sample_{}'.format(0))
    gr.create_dataset('x_t', (hf.attrs['ns'], int(tf / ts)), dtype='f', data=dataX,
                      compression="lzf")
    gr.create_dataset('u_t', (hf.attrs['nu']+1, int(tf / ts)), dtype='f', data=dataU, compression="lzf")

    hf.close()

    # Plot profile
    x = {name: output[:, i] for name, i in zip(names, range(output.shape[1]))}
    fig = plt.figure(1)
    ax = fig.subplots(2)
    ax[0].plot(tim, x['CA[{t}]'])
    ax[1].plot(tim, x['Tr[{t}]'])

    print('Done.')
