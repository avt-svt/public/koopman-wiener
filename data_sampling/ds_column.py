import pyomo.environ as pyomo
import pyomo.dae as pydae
import numpy as np
from numpy import matlib
import matplotlib.pyplot as plt
import h5py

plt.ion()  # Show plots immediately
np.random.seed(99)


class DistillationColumn:
    """
    Model information see: https://doi.org/10.1016/S0959-1524(99)00055-4
    """
    def __init__(self, tsim=100.0):
        self.model = self.createModel(tf=tsim)
        self.inputs = pyomo.Suffix(direction=pyomo.Suffix.LOCAL)

    def setInputs(self, u):
        m = self.model
        for key in u:
            if key == 't':
                pass
            elif key == 'L':
                self.inputs[m.L] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
            elif key == 'F':
                self.inputs[m.F] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
            elif key == 'V':
                self.inputs[m.V] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
            elif key == 'x_f':
                self.inputs[m.x_f] = {i_: j_ for i_, j_ in zip(u['t'], u[key])}
            else:
                assert False

    def setParams(self, p):
        m = self.model
        for key in p:
            if key == 'L':
                self.inputs[m.L] = {0: p[key]}
            elif key == 'F':
                self.inputs[m.F] = {0: p[key]}
            elif key == 'V':
                self.inputs[m.V] = {0: p[key]}
            elif key == 'x_f':
                self.inputs[m.x_f] = {0: p[key]}
            else:
                assert False

    def simulateModel(self, initCond, ctr, param, ts=1.0):
        self.setInputs(ctr)
        self.setParams(param)
        opts = {'reltol': 1e-10, 'abstol': 1e-10}
        sim = pydae.Simulator(self.model, package="casadi")

        tt, xx = sim.simulate(tstep=ts, integrator="idas", initcon=initCond,
                              varying_inputs=self.inputs, integrator_options=opts)

        nn = list(map(str, sim.get_variable_order(vartype=None) + sim.get_variable_order(vartype='algebraic')))
        return tt, xx, nn

    @staticmethod
    def createModel(tf):
        model = pyomo.ConcreteModel()

        # Continuous Time Set
        t = model.t = pydae.ContinuousSet(bounds=(0.0, tf))  # sec

        # Pyomo Sets
        N_t = model.trays = pyomo.RangeSet(1, 10)
        model.trays_s = pyomo.RangeSet(2, 4)  # Stripping section
        model.trays_r = pyomo.RangeSet(6, 9)  # Rectifying section

        # Physical Parameters
        model.alpha_ = pyomo.Param(initialize=3.55, doc="-")  # Relative volatility
        # alpha = 3.5
        model.M = pyomo.Param(initialize=0.5, mutable=False, doc="kmol")  # Constant holdup

        # External Inputs
        model.x_f = pyomo.Var(t, initialize=0.5)
        model.F = pyomo.Var(t, initialize=0.1)
        model.V = pyomo.Var(t, initialize=2.0, bounds=(0.01, 0.1), doc="kmol/s")
        model.L = pyomo.Var(t, initialize=1.0, bounds=(0.005, 0.05), doc="kmol/s")

        # Variables
        model.x = pyomo.Var(N_t, t, bounds=(0, 1))
        model.y = pyomo.Var(N_t, t, bounds=(0, 1))

        # Time derivatives
        model.dxdt = pydae.DerivativeVar(model.x)

        # Differential balance equation
        # n=1: Bottom stage
        # n=nT: Top stage
        # n=nT+1: Condenser
        def balance(m, n, tt):
            if n == 10:
                # Condenser
                return m.M * m.dxdt[n, tt] == m.V[tt] * m.y[n - 1, tt] \
                                              - m.V[tt] * m.x[n, tt]

            elif n in m.trays_r:
                return m.M * m.dxdt[n, tt] == m.L[tt] * (m.x[n + 1, tt] - m.x[n, tt]) \
                                              + m.V[tt] * (m.y[n - 1, tt] - m.y[n, tt])

            elif n == 5:
                # Feed tray
                return m.M * m.dxdt[n, tt] == m.L[tt] * m.x[n + 1, tt] \
                                              - (m.L[tt] + m.F[tt]) * m.x[n, tt] \
                                              + m.V[tt] * (m.y[n - 1, tt] - m.y[n, tt]) \
                                              + m.F[tt] * m.x_f[tt]

            elif n in m.trays_s:
                return m.M * m.dxdt[n, tt] == (m.L[tt] + m.F[tt]) * (m.x[n + 1, tt] - m.x[n, tt]) \
                                              + m.V[tt] * (m.y[n - 1, tt] - m.y[n, tt])

            else:
                # Bottom tray
                return m.M * m.dxdt[n, tt] == (m.L[tt] + m.F[tt]) * m.x[n + 1, tt] \
                                              - (m.L[tt] + m.F[tt] - m.V[tt]) * m.x[n, tt] \
                                              - m.V[tt] * m.y[n, tt]

        model.ode = pyomo.Constraint(N_t, t, rule=balance)

        # Substitution of VLE
        def vle(m, n, tt):
            return m.y[n, tt] * (1 + (m.alpha_ - 1) * m.x[n, tt]) == m.alpha_ * m.x[n, tt]
        model.vle = pyomo.Constraint(N_t, t, rule=vle)

        return model


if __name__ == "__main__":
    ts = 1.0  # min
    generate_test_set = False
    if generate_test_set:
        nstep = 25
        tstep = 0.2 * 60  # hrs
        hf = h5py.File('./ds_pearson_test.h5', 'w')
    else:
        nstep = 400
        tstep = 1.0 * 60  # hrs
        hf = h5py.File('./ds_pearson.h5', 'w')
    assert np.sqrt(nstep) % 1 < 1e-15
    assert tstep % ts < 1e-15
    tf = int(nstep * tstep)

    m1 = DistillationColumn(tsim=tf)

    shuf = np.arange(nstep)
    np.random.shuffle(shuf)

    hf.attrs['ts'] = ts
    hf.attrs['ns'] = 10  # no of states
    hf.attrs['nu'] = 2  # no of controls

    g = hf.create_group('scenario_{}'.format(0))

    x0 = np.array([4.6689869548e-01, 6.7762000448e-01, 7.6866148149e-01, 7.9777047061e-01, 8.0613781635e-01,
                   9.2759222396e-01, 9.7551017266e-01, 9.9207421789e-01, 9.9753327166e-01, 9.9930391357e-01])

    L_range = [1.55, 1.95]
    x_f_range = [0.5, 0.7]

    if generate_test_set:
        Luni = np.random.uniform(L_range[0], L_range[1], nstep)
        xuni = np.random.uniform(x_f_range[0], x_f_range[1], nstep)
    else:
        Luni_ = np.matlib.repmat(np.linspace(L_range[0], L_range[1], int(np.sqrt(nstep))),
                                 1, int(np.sqrt(nstep)))[0, :]
        xuni_ = np.reshape(np.transpose(np.matlib.repmat(np.linspace(x_f_range[0], x_f_range[1],
                                                                     int(np.sqrt(nstep))),
                                                         int(np.sqrt(nstep)), 1), [1, 0]), [1, -1])[0, :]
        Luni = Luni_[shuf]
        xuni = xuni_[shuf]

    controls = {'t': np.arange(start=0, stop=tf, step=tstep), 'L': Luni, 'x_f': xuni}
    parameters = {'V': 2.0, 'F': 1.0}

    tim, output, names = m1.simulateModel(x0, controls, parameters, ts=ts)

    dataX = output[:, :10].transpose()

    datalogX_s = np.log10(dataX[:5, :])
    datalogX_r = np.log10(1 - dataX[5:, :])
    dataX = np.concatenate([datalogX_s, datalogX_r], axis=0)

    dataU = np.vstack([np.arange(0, tf, ts),
                       np.reshape(np.transpose(np.matlib.repmat(controls['L'], int(tstep / ts), 1), (1, 0)), (1, -1)),
                       np.reshape(np.transpose(np.matlib.repmat(controls['x_f'], int(tstep / ts), 1), (1, 0)), (1, -1))])

    gr = g.create_group('sample_{}'.format(0))
    gr.create_dataset('x_t', (hf.attrs['ns'], int(tf / ts)), dtype='f', data=dataX,
                      compression="lzf")  # compression="gzip"
    gr.create_dataset('u_t', (hf.attrs['nu'] + 1, int(tf / ts)), dtype='f', data=dataU, compression="lzf")

    hf.close()

    # Plot profile
    x = {name: output[:, i] for name, i in zip(names, range(output.shape[1]))}
    fig = plt.figure(1)
    ax = fig.subplots(2)
    ax[0].semilogy(tim, x['x[1,{t}]'])
    ax[1].semilogy(tim, 1 - x['x[10,{t}]'])

    print('Done.')

